#+TITLE: Ciberseguridad 101
#+AUTHOR: Alvar Maciel
#+DATE: Primavera 2021
#+OPTIONS: timestamp:nil
#+EMAIL: alvarmaciel@gmail.com
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+REVEAL_THEME: moon
#+STARTUP: noinlineimages
#+OPTIONS: reveal_width:1400 reveal_height:1000
#+OPTIONS: toc:1
#+REVEAL_MIN_SCALE: 1.0
#+REVEAL_MAX_SCALE: 1.0
#+REVEAL_PLUGINS: (notes search zoom)
#+REVEAL_MARGIN: 0.1
#+REVEAL_TITLE_SLIDE: <h1>%t</h1><h2>%s</h2><h3>%A %a</h3><p>Link a los Slides: <a href="%u">%u</a></p><p>%d</p>
#+REVEAL_TALK_URL: https://bit.ly/cibersec101

* Derechos de la ciudadanía
#+begin_quote
Los ciudadanos tienen derecho a la *libertad de expresión*, al *acceso a la información*, a la *privacidad de las comunicaciones* y a la *seguridad de sus datos*.
#+end_quote

* Reconocimiento del contexto de la CiberSeguridad (Activos)
#+begin_quote
El campo de la ciberseguridad es muy amplio, podemos pensar en niveles o capas de intervención de los atacantes y de defensa.
#+end_quote
#+ATTR_REVEAL: :frag t
*Algunos de los objetivos de los ataques*
#+ATTR_REVEAL: :frag (appear)
- Sistemas,
- servidores,
- hosts,
- redes,
- usuarios...
* Ataques a  Usuarios

** Ataques a contraseñas

#+begin_quote
Consiste en adivinar nuestra contraseña a base de  ensayo y error. Los atacantes comienzan probando diferentes combinaciones con nuestros datos personales,  en caso de conocerlos por otras vías. Luego, continúan haciendo combinaciones de palabras al azar, conjugando  nombres, letras y números, hasta que dan con el patrón correcto.
#+end_quote

** Ataques por Ingeniería Social

#+begin_quote
Los ataques por ingeniería social se basan en un conjunto de técnicas dirigidas a nosotros, los usuarios, con  el objetivo de conseguir que revelemos información  personal o permita al atacante tomar control de nuestros  dispositivos. suelen utilizarse como paso  previo a un ataque por malware.
#+end_quote

*** Phishing. Vishing, Smishing
#+begin_quote
Se tratan de tres ataques basados en ingeniería social  muy similares en su ejecución. De forma general, el  ciberdelincuente enviará un mensaje suplantando a una  entidad legítima, como puede ser un banco, una red social, un  servicio técnico o una entidad pública, con la que nos sintamos  confiados, para lograr su objetivo. Estos mensajes suelen ser  de carácter urgente o atractivo, para evitar que apliquen el  sentido común y se lo piensen dos veces.
#+end_quote
#+REVEAL: split

*Phishing*
- Suele emplearse el correo electrónico, redes sociales o aplicaciones de mensajería instantánea.
[[file:bandejaEntrada.png]]

#+REVEAL: split
*El mail del "banco Superville"
[[file:pishing.png]]
#+REVEAL: split
*Vishing*
- Se lleva a cabo mediante llamadas de teléfono.
*Smishing*
- El canal utilizado son los SMS.

** Como protegernos

Para evitar Este tipo de fraude, que suele ejecutarse tras acceder a links o enlaces enviados por mensajes y correos, dejamos algunos consejos:
#+ATTR_REVEAL: :frag (appear)
- No abras correos que no esperabas recibir. Mirá la dirección del mail y chequeá cada letra para ver si es la que figura en la página oficial de tu banco u organización conocida.
- o abras links de mensajes mal redactados, con fallas ortográficas o sintácticas, ni descargues sus archivos porque podrían contener un código malicioso.
- Tampoco realices ninguna acción si recibís un correo que te indica actuar de forma inmediata.
- No reveles nunca las claves personales.
- En caso de recibir un mail institucional desde una casilla general (como Gmail, Outlook o Yahoo), mejor desconfiar.

* Ataques a Redes y Servicios
#+begin_quote
Generalmente, este tipo de ataques se basan en  interponerse en el *intercambio de información entre  nosotros y el servicio web, para monitorizar y robar* datos  personales, bancarios, contraseñas, etc.
#+end_quote
** Redes Trampa
#+begin_quote
Consiste en la creación de una red wifi gemela a otra legítima y segura, con un nombre igual o muy similar a la original, que crean utilizando software y hardware.
El objetivo es conseguir nuestros datos bancarios, de redes o correos pensando que estamos en una conexión segura.
Suele darse en lugares con una red wifi pública
#+end_quote
*** Como protegerse

#+ATTR_REVEAL: :frag (appear)
- Aprendiendo a identificar las redes wifi falsas:
  - El primer indicativo es que existan dos redes con nombres iguales o muy similares.
  - Chequear que las webs a las que accedes tras conectarte solo utilizan el protocolo *https*

    [[file:protocoloDeConexion.png]]
#+REVEAL: split
  - Es probable que estas redes estén abiertas o que permitan introducir cualquier contraseña.
  - Desconectar  la  función  del  dispositivo  móvil  para  conectarse automáticamente  a  redes  abiertas.
** Spoofing
#+begin_quote
Consiste en el empleo de una serie de técnicas para para suplantar nuestra identidad, la de una web o una entidad.
*El objetivo* de los atacantes es, mediante esta  suplantación, *disponer de un acceso a nuestros datos*
#+end_quote
*** IP Spoofing
#+begin_quote
El atacante consigue falsear su dirección IP y hacerla pasar por una dirección distinta.
Uno de los objetivos es obtener acceso a redes con autenticación de usuarios, o con permisos en función de las IPs. Se suele utilizar como paso intermedio para ataques DDoS
#+end_quote
#+REVEAL: split
*Como protegerse*
- Configurar el router de forma segura haciendo un filtrado de direcciones IP para controlar las conexiones entrantes.
- [[https://www.osi.es/es/actualidad/blog/2016/11/03/tu-router-tu-castillo-medidas-basicas-para-su-proteccion][Medidas básicas de protección del router]]
*** Web Spoofing
#+begin_quote
Consiste en la suplantación de una página web real por otra falsa. La web falsa es una copia del diseño de la original, llegando incluso a utilizar una URL muy similar. El atacante trata de hacernos creer que la web falsa es la original.
#+end_quote
#+REVEAL: split
*Como protegerse*
- Revisar con mucho cuidado la URL para identificar diferencias con la original
[[file:protocoloDeConexion.png]]
*** Email Spoofing
#+begin_quote
Consiste en suplantar la dirección de correo de una persona o entidad de confianza.
#+end_quote

** Otros ataques comunes:
- DNS Spoofing
- Ataques a Cookies
- Ataques DDoS
- Inyección SQL
- Escaneo de puertos
- Man in the middle
- Sniffing
* Ataques a por Malware
#+begin_quote
Los ataques por malware se sirven de programas maliciosos cuya funcionalidad consiste en llevar a cabo acciones dañinas en un sistema informático y contra nuestra privacidad. Generalmente, buscan robar información, causar daños en el equipo, obtener un beneficio económico a nuestra costa o tomar el control de su equipo.
#+end_quote
#+REVEAL: split
#+begin_quote
Hay muchos tipos de Malware pero las medidas de protección son siempre las mismas:
- Mantener actualizado el SO
- Mantener herramientas antimalware actualizadas
- Usar sistemas operativos que tengan capas de seguridad más activas (GNU/Linux o SO inmutables)
#+end_quote
** Tipos de Malware
- Virus
- Adware
- Spyware
- Troyanos
- Backdoors
- Keyloggers
- Stealers
- RamsomWare
- Worms
- Rootkit
- Botnets
- muchooos más....

* Lo privado, lo público y lo íntimo
#+begin_quote
Hablemos un poco sobre lo *PÚBLICO* lo *PRIVADO* y lo *ÍNTIMO*
#+end_quote
#+REVEAL: split
[[file:snowdenPrivacidad.png]]

* Créditos
La información de esta charla puede ampliarse en estos enlaces:
- [[https://www.argentina.gob.ar/noticias/como-evitar-el-fraude-por-suplantacion-de-identidad-en-internet-0][Dirección Nacional de Ciberseguridad: Como evitar el fraude por susplantación de identidad]]
- [[https://www.osi.es/sites/default/files/docs/guia-ciberataques/osi-guia-ciberataques.pdf][Guía de Ciberataques - Oficina de seguridad del internauta]]
- [[https://www.argentina.gob.ar/jefatura/innovacion-publica/ssetic/direccion-nacional-ciberseguridad/informes-de-la-direccion-0][Bootnets una guía para el usario]]
- [[https://www.argentina.gob.ar/jefatura/innovacion-publica/direccion-nacional-ciberseguridad/recomendaciones][Recomendaciones de ciberseguridad]]
- [[https://www.argentina.gob.ar/noticias/ciberseguridad-crearon-una-guia-de-buenas-practicas-para-el-desarrollo-de-aplicaciones-web][Guía de buenas prácticas para el desarrollo de una aplicación web]]
#+begin_quote
Muchas Gracias
#+end_quote
