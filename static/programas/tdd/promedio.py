def ingresar_nombre():
    nombre = input("Ingrese un nombre: ")
    return nombre


def ingresar_numero():
    numero = int(input("Ingrese un número: "))
    return numero


def sumar_numeros(num1, num2, num3):
    resultado = num1 + num2 + num3
    return resultado


def promedio_de_tres_numeros(num1, num2, num3):
    resultado = (num1 + num2 + num3) / 3
    # 2resultado = sumar_numeros(num1, num2, num3) / 3
    return resultado


if __name__ == "__main__":
    ingresar_nombre()
    print(
        promedio_de_tres_numeros(
            ingresar_numero(), ingresar_numero(), ingresar_numero()
        )
    )
