from selenium import webdriver
import unittest


class NuevaVisitaLaPagina(unittest.TestCase):
    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def test_puedo_abrir_la_pagina(self):
        self.browser("file:///home/alvar/git/escuelas-proa/static/images/logo.html")
        self.assertIn("Inkscape", self.browser.title)


if __name__ == "__main__":
    unittest.main()
