import unittest
import promedio


class PromedioDeTresNotas(unittest.TestCase):
    def setUp(self):
        self.nombre = "toto"
        self.num1 = 4
        self.num2 = 5
        self.num3 = 7

    def test_puedo_ingresar_un_nombre(self):
        self.assertEqual(promedio.ingresar_nombre(), self.nombre)

    def test_puedo_ingresar_un_numero(self):
        self.assertTrue(type(promedio.ingresar_numero()) is int)

    def test_puedo_sumar_numeros(self):
        self.assertTrue(promedio.sumar_numeros(self.num1, self.num2, self.num3), 15)

    def test_puedo_sacar_promedio(self):
        self.assertTrue(
            promedio.promedio_de_tres_numeros(self.num1, self.num2, self.num3), 5.0
        )


if __name__ == "__main__":
    unittest.main()
