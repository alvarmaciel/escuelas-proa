+++
title= "Test Driven Development - TDD"
date= 2021-11-15T10:42:36-03:00
draft= false
tags = ['testing', 'python', 'tutorias', 'programacion' ]
+++

Este documento contiene aspectos teóricos y una secuencia de ejercicios para entender la teoría detrás del desarrollo dirigido por test.
Fuentes: 
- [Unit Testing in Python - James Bucker](https://cpske.github.io/ISP/testing/PythonUnitTesting.pdf)
- [Taller de Python](https://aulasoftwarelibre.github.io/taller-de-python/Testing/TDD/)
- [Obey The Testing Goat](http://www.obeythetestinggoat.com)

Este es el ciclo de desarrollo
![](/images/test-driven-development-TDD.webp)

1. Escribir un test que falla
2. Hacer que pase el test
3. Refactorizar
4. Volver a empezar 


>El desarrollo guiado por pruebas de software, o Test-driven development (TDD) es una práctica de ingeniería de software que involucra otras dos prácticas: Escribir las pruebas primero (Test First Development) y Refactorización (Refactoring). Para escribir las pruebas generalmente se utilizan las pruebas unitarias (unit test en inglés). En primer lugar, se escribe una prueba y se verifica que la nueva prueba falla. A continuación, se implementa el código que hace que la prueba pase satisfactoriamente y seguidamente se refactoriza el código escrito. El propósito del desarrollo guiado por pruebas es lograr un código limpio que funcione. La idea es que los requisitos sean traducidos a pruebas, de este modo, cuando las pruebas pasen se garantizará que el software cumple con los requisitos que se han establecido.

## Creando y testeando una aplicación simple.
Nos pideron que hagamos una aplicación clásica. Un programa que nos permita ingresar un nombre, tres notas y sacar su promedio. Vamos a realizar la programación de este clásico, siguiendo al pie de al letra TDD.
En este caso haremos varios **Unit Test**. Estos son test que se aplican a la funcionalidad del sistema y desde el punto de vista del programador. 
Existen otros tipos de test. Los **test funcionales** que se aplican desde "afuera" de la aplicación. Desde el punto de vista del usuario.

### Manos a la Obra

Para empezar a programar un test en Python, primero hay que decidir que framework vamos a usar. Para esta prueba usaremos [unittest](https://docs.python.org/3/library/unittest.html). Hay otros, pero este es que framework que viene por default. Cada Lenguaje tiene uno o más marcos de desarrollo de testing.

1. Crear un directorio de trabajo, donde correremos los test y nuestro programa a testear.
2. Crear un archivo en ese directorio llamado `unit_tes.py``
4. En este archivo escribir:
```python
import unittest
import promedio

class PromedioDeTresNotas(unittest.TestCase):
    def setUp(self):
        self.nombre = "toto"

    def test_puedo_ingresar_un_nombre(self):
        self.assertEqual(promedio.ingresar_nombre(), self.nombre)


if __name__ == "__main__":
    unittest.main()

```
Ya tenemos nuestro test, corrámoslo desde la terminal con `python unit_test.py`

Y analicemos el error.

Ahora es tiempo de escribir un archivo para que pase nuestro primer test.

1. Crea un archivo que se llame `promedio.py`
2. En él tipea:

```python
def ingresar_nombre():
    pass


if __name__ == "main":
    pass
```

