+++
title= "Estudio de funciones y estructuras repetitivas"
date= 2021-11-08T23:31:01-03:00
draft= false
tags= ['programacion', 'python']
weight = 2
+++


> Material adaptado de [Think Python 2](https://greenteapress.com/thinkpython2/)

Para hacer este estudio vamos a usar el modulo `turtle` de Python y realizaremos ejercicios para diseñar funciones que trabajen juntas.




## El módulo Turtle

Python trae muchos módulos integrados que nos permiten ampliar las posibilidades de lo que programamos. Turtle es uno de ellos. Para chequear que tienen el módulo `turtle` abran el interprete de comandos y tipeen:

```
>>> import turtle
>>> manu = turtle.Turtle()
```
Cuando corran esto se abrirá una ventana nueva con una pequeña flecha que representa una tortuga. Cierren la ventana.




## Manos a la obra

Creen una archivo llamado mipoligono.py y tipeen en él el siguiente código:

```python
import turtle
manu = turtle.Turtle()
print(manu)
turtle.mainloop()
```



### Todes juntes

1.  ¿Qué ocurrió?
2.  Escribir un comentario al lado de cada línea explicando que hace cada sentencias




## Manos a la obra

Ahora vamos por más. Agrega estas lineas debajo del `print(manu)`

```python
manu.fd(100)
manu.lt(90)
```



### Todes juntes

1.  ¿Qué ocurrió?
2.  Escribir un comentario al lado de cada línea explicando que hace cada sentencias.




## Manos a la obra

Ahora modifiquen el programa para que dibuje un cuadrado.


# Repeticiones

La sentencia `for` nos permite repetir el código que está dentro de su alcance. Este alcance está definido por el lugar en el que escribimos el código, es decir su nivel de identacion.
Por ejemplo:

```python
for i in range (4):
    print ('Hola chiques!')
```

Como ven el resultado es que me imprimió 4 veces `Hola chiques!`.

**Una posible descripción de programa en pseudocódigo podría ser esta:**
1.   Para cada valor de `i` en un rango de (4):
2. imprimir(&rsquo;Hola chiques!&rsquo;)

- Entonces:
 -   Imprime una vez y el valor de  `i=1`
 -   Imprime una vez y el valor de `i=2`
 -   Y lo repite hasta que `i=4`




## Manos a la obra

Ahora modificá el porgrama para que dibuje un cuadrado pero usando la sentencia `for`


<a id="org758eaa0"></a>

# Funciones

Las funciones son bloques de código que se pueden llamar en cualquier momento y ejecutan lo que está en él. Estas funciones tienen un nombre, y aceptan **parametros** esto es, datos que podemos pasarle. También las funciones nos pueden **devolver** datos. Veamos un ejemplo.


<a id="orga722ae6"></a>

## Manos a la obra

Copien este código en un archivo llamado funciones.py

```python
def escribir_nombre(nombre):
    return print(f'Hola {nombre}')
    
escribir_nombre("manuelita")
```

<a id="org5123d35"></a>

### Todes Juntes

1.  ¿Qué ocurrió?
2.  Escribir un comentario al lado de cada línea explicando que hace cada sentencias.


<a id="orgc7c43bb"></a>

## Manos a la obra

1.  Escribir una función llamada `cuadrado` que tome un parámetro llamado `t`, que será una tortuga. Tiene que usar la tortuga para dibujar un cuadrado. Luego escriban la **llamada a la función** que pase `manu` como parámetro de `cuadrado` y corran el programa.
2.  Agreguen otro parámetro llamado `distancia` a `cuadrado`. Modifiquen el cuerpo de la función de forma que el largo del cuadrado sea `distancia`. Y luego modifiquen la llamada a la función para pasar el segundo argumento. Prueben el programa con varios valores



