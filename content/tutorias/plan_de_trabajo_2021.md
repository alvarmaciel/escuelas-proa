+++
title= "Plan de trabajo 2021"
date= 2021-11-08T22:38:03-03:00
draft= false
weight = 1
+++

{{% notice info %}}
[Propuesta de trabajo inicial. Junio 2021](https://docs.google.com/document/d/1vRh334AjCW07fUqaJ9DmYMW8ClQqYgKbXKAHP31xbrE/edit?usp=sharing)
{{% /notice %}}

## Plan de trabajo último bimestre 2021

A partir del relevamiento realizado durante los meses de Agosto y Septiembre, se evalúa junto a la conducción, realizar acciones en torno a las materias de Orientación de la escuela.

Dada la dificultad para sostener aprendizajes significativos que plantea la pandemia de covid-19. Nos proponemos habilitar espacios de encuentros entre los estudiantes, el contenido y el docente como agente de enseñanza.

### Objetivos generales
- Ciclar estudiantes en riesgo de 3er año
- Acompañar en el cierre de escolaridad media a estudiantes de 6t0 2021
- Fortalecer, en el marco de la orientación, a los estudiantes en riesgo de 1er y 4to año

#### Objetivos específicos
- Fortalecer los conceptos de función, variable, estructuras repetitivas y estructuras condicionales en 3ro, 4to y 6to año.
- Experimentar el uso de lenguajes de programación de bloques con 1er año.

### Tiempos:
- Octubre
- Noviembre
- Diciembre

Los tiempos fueron cambiando a medida que se agregaron horas de cursada, se unificaron burbujas o se volvió a la presencialidad plena. al 1 de noviembre esta es la estructura

- Lunes:
  - Primer Turno:
    - Sexto 2020 (preapración para testing)
  - Segundo Turno
    - Tercero
    - Sexto 2021
- Viernes
  - Primer turno
    - Primero
  - Segundo turno
    - Cuarto 
  - Tercer Turno
    - Tercero

### Turno de autoevaluación con indicadores

Se propone esta grilla de evaluación a los estudiantes para que sepan que contenidos evaluaremos y como se hará.
Cada 2 encuentros vamos a evaluar algunos de los indicadores especificados en la grilla

| criterios/<br>indicadores | Logrado | En Proceso | No alcanzados |
|---|---|---|---|
| Estructuras repetitivas | Uso la sentencia for en la resolución del problema de forma eficiente, <br>sintetizando el algoritmo y sin repetir patrones | Usa la sentencia for en la resolución del problema repitiendo <br>algunos patrones que pueden ser sintetizados | No uso la sentencia for |
| Variables | Resuelvo utilizando variables<br>y poniendo nombres significativos | Resuelvo utilizando variables pero aun<br>no entiendo bien que son.  | No se lo que es una variable |
| Uso de funciones | Utilizo funciones para<br>organizar el desarrollo de<br>la solución a los problemas<br>incorporando mas de un argumento | Utilizo funciones para<br>organizar el desarrollo de<br>la solución a los problemas<br>pero no comprendo como pasar argumentos | No uso funciones para<br>la resolución de problemas |
