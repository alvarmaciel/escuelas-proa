---
title: "Plan de Estudio 2021"
date: 2021-11-25T18:46:28-03:00
draft: false
tags: [cifrado, python]
weight : 1
---

En esta propuesta se les propondrá a los estudiantes desafíos criptográficos para abordar la estructura de los datos.
El enfoque didáctico que sustenta la propuesta está centrado en el construccionismo<sup>[1](#nota-al-pie-1)</sup> como aporte teórico fundamental, el aprendizaje por proyectos y el aprendizaje por indagación. Estos enfoques no cancelan las demostraciones, pero sí ponen acento en la exploración por parte de los estudiantes. Esta exploración no solo es de las herramientas, sino también de las resoluciones mismas de los problemas.

Dado lo avanzado del año y la situación de alternancia en pandemia, se propone un proyecto reducido y que intenta generalizar el contenido de la materia a partir de un escenario lúdificado que lleve a los estudiantes a resolver distintos desafíos.
El proyecto implica presentar a los estudiantes parte de la historia de la criptografía y realizar desafíos de codificación y decodificación sencillos.

<!--truncate-->

## Síntesis de la secuencia.

- Duración del Proyecto: 6 a 8 semanas

### Objetivos:

- Extraer características comunes de un proceso o conjunto de procesos.
- Crear dispositivos/productos computacionales.
- Testing y Debugging
- Desarrollo de habilidades cooperativas y trabajo en equipo, .

### Aprendizajes involucrados

- Comprender que la decisión del tipo o estructura de dato a utilizar depende del tipo de problema a resolver.
- Comprender el rol de los parámetros.
- Incorporar bibliotecas necesarias para la resolución de problemas planteados.
- Recuperar las estructuras de datos arreglos y matrices.
- Recuperar los conceptos de índice y contenido. Diferenciarlos.
- Identificar similitudes y diferencias entre arreglos y matrices.
- Comprender e identificar la importancia de inicialización de los valores de las matrices y arreglos.
- Crear programas que recorran, modifiquen o filtren contenido de matrices y arreglos.
- Utilizar ciclos anidados.
- Identificar errores de sintaxis en diferentes programas según el lenguaje utilizado.
- Poder identificar, ante la ejecución de un programa secuencial sencillo, si funciona o no correctamente, teniendo en cuenta el objetivo del programa creado.

### Estrategias didácticas que se pondrán en juego:

- Bitácora del proyecto
- Resolución de problemas grupales e individuales
- Confección grupal de Rúbricas de evaluación
- Confianza en la capacidad de aprendizaje de los y las estudiantes

## Secuencia de actividades previstas

| Etapa                               | Actividades                                                                                                                             | Evaluación                                                   |
| ----------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------ |
| Presentación                        | - Presentación de proyecto, los objetivos, qué y cómo se evaluará.<br/>- Definición del lenguaje, definición de IDE y su configuración. |                                                              |
| Cifrado por diámetro                | - Presentación de la escítala espartana<br/- Desafío 1: Cifrado espartano                                                               |                                                              |
| Cifrado por diámetro                | - Desafío 1: Cifrado espartano<br/>- Programa de descifrado del mensaje                                                                 | Lectura : Historia de la criptografía - Apunte del profesor. |
| Cifrado por diámetro                | - Desafío 1: Cifrado espartano <br/>- Programa de cifrado del mensaje                                                                   | Evaluación entre pares de los test.                          |
| Cifrado por sustitución             | - Presentación del cifrado por sustitución. Desafío 1. cifrado Cesar                                                                    |                                                              |
| Cifrado por sustitución             | Cifrado César con uso de array simple                                                                                                   |                                                              |
| Cifrado por sustitución             | Cifrado César con uso de tabla ascii - Uso de ciclos.                                                                                   | - Evaluación entre pares de los test.                        |
| Cifrado por sustitución Hexadecimal | - Presentación de sistema hexadecimal, relación con tabla ascii.<br/>- Descifrado de string hexadecimal                                 |                                                              |
| Cifrado por sustitución Hexadecimal | - Presentación de sistema hexadecimal, relación con tabla ascii.<br/>- Descifrado de string hexadecimal                                 |                                                              |

<a name="nota-al-pie-1">[1]</a>: Ver: <a hfre="https://alvarmaciel.gitlab.io/cyberiada/post/construccionismo-vs-instruccionismo1/">Construccionismo vs Instruccionismo</a> de Seymour Papert. Traducción propia.
