---
title: "Cadena de caracteres"
date: 2021-11-08T23:31:01-03:00
draft: false
tags: ['programacion', 'python']
weight: 5
---

> Este es un resumen con actividades escrito a a partir de [Strings and Characters Data in Python](https://realpython.com/courses/python-strings/) y algunos capítulos de [Think Python: How to Think Like a Computer Scientist](https://greenteapress.com/thinkpython2/html/index.html)


## Operadores
Los `strings` como venimos viendo son objetos que contienen secuencias de caracteres.
Como con los números, se pueden aplicar **operadores** numéricos a los strings

Estos son los operadores con los que vamos a practicar:

- El operador `+` concatena strings:

Probá este código en el shell de Python 🚀

```python
infusion = "café"
colacion = "Tostada"
gustito = "Huevos con panceta"

infusion  + colacion

infusion + colacion + gustito

```

¿Qué resultado obtuviste? 🗒️


- El operador `*` crea múltiples copias de un string

Probá este código en el shell de Python 🚀

```python
texto = "spam"
n = 2

s * n
n = 8
s * n

```

¿Qué resultado obtuviste? 🗒️

- Los operadores `in` y `not` proveen pruebas lógicas. Y nos dicen si está o no está la cadena buscada en el string

Probá este código en el shell de Python 🚀

```python
texto = "spam"

texto in "Me llegó un spam otra vez"

texto in "Tengo correo nuevo"

texto not in "Tengo correo nuevo"

texto not in "Me llegó in spam otra vez"
```

## Funciones propias de Python

Python provee muchas funciones para operar con los strings. Algunas funcionan con strings y otras con caracteres

|Función|Descripción|
|-------|-----------|
|chr()|Convierte un número entero (int) a su carácter en la tabla Unicode|
|ord()|Convierte un carácter en su número entero de la tabla Unicode|
|len()| devuelve el largo de un string|
|str()| devuelve la representación string de un objeto. Por ejemplo, cambia el tipo de dato de entero a string en un número|


Contestá 🗒️:

1. Qué carácter corresponde a este número: `64`
2. Qué número de la tabla unicode corresponde a este carácter: `#``
3. Cual es el largo de este string : `Complejo es mejor que complicado.`
4. Qué función usarías u como la usarías para convertir este número en un string: `42`
