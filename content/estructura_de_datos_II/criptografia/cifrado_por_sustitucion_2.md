+++
title= "Cifrado por sustitución II"
date= 2021-11-25T19:11:45-03:00
draft= false
weight= 6
disableToc= false
+++

## Preparación 

A partir de todo lo que venimos viendo ya es hora de que en equipos realicen un programa.

Pasos a seguir:
1. Abrir un entorno de desarrollo, preferentemente el IDLE que es el entorno de desarrollo integrado de Python. Para futuras referencias les dejo la sección del [manual en castellano del IDLE](https://docs.python.org/es/3/library/idle.html "Link al manual en castellano del IDLE"). Lo primero que se abre es la Shell, un interprete de comandos línea a línea muy bueno para hacer pruebas
   ![](/images/idle_1.png)
2. Ir a File -> New File para abrir un archivo de edición.
   ![](/images/idle_2.png)
3. Este es el editor de texto del entorno de desarrollo, acá escriben sus programas.
   ![](/images/idle_3.png)
4. Para ejecutar el programa, ir a Run -> Run Module.
   ![](/images/idle_4.png)
5. Este es el resultado de la ejecución de ese programa
   ![](/images/idle_5.png)
## Activdad 1 - Identificar error
1. Este otro programa tiene un error. 

🛠️ **En tu carpeta escribí cual es la causa del error y como se te ocurre arreglarlo**  🛠️
![](/images/idle_error.png)




## Actividad 2 - Recorrer un String y cambiar sus vocales

### Repeticiones y condicionales

La sentencia `for` nos permite repetir el código que está dentro de su alcance. Este alcance está definido por el lugar en el que escribimos el código, es decir su nivel de indentación.


Por ejemplo:

```python
nombre = "ANGELES MARTINEZ"

vocales_A_E = 0

for letra in nombre:
    if letra == "A":
        vocales_A_E = vocales_A_E + 1
    elif letra == "E":
        vocales_A_E = vocales_A_E + 1

print(f'En {nombre} hay {vocales_A_E} vocales')
        
```

**Una posible descripción de programa en pseudocódigo podría ser esta:**
1. Por cada `letra` en un la variable `nombre`:
   1. Si el valor de `letra` es igual a `"A"`
      1. sumar 1 a la variable `vocales_A_E`
   2. en caso de que `letra` sea igual a `"E"`
      1. sumar 1 a la variable `vocales_A_E`
2. Imprimir en `nombre` hay la cantidad de vocales que se calculó y guardó en `vocales_A_E`

1. 🛠️ **En tu carpeta escribí, en pseudocódigo, un algoritmo para contar las vocales de cualquier nombre utilizando `for` como estrategia de repetición e `if` y `elif` como condicionales**  🛠️
2. **🛠️ Escribí el programa en Python de tu pseudocódigo 🛠️**
