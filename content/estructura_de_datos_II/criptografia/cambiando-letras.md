---
title: "Cambiando letras por números"
date: 2021-11-08T23:31:01-03:00
draft: false
tags: ['programacion', 'python']
weight: 4
---

Como vimos en el encuentro anterior, podemos cifrar cambiando una letra por otra.
Ahora vamos a cambiar una letra por un número. Específicamente, la vocal por un número. Esta va a ser nuestra clave

| A   | E   | I   | O   | U   |
| --- | --- | --- | --- | --- |
| 4   | 3   | 1   | 0   | 8   |

## Vamos por partes

### variables

Las variables nos permiten guardar datos para después recuperarla, usarla o modificarla. En Python, la definición de las variables y su asignación se pueden hacer en el mismo momento:

```python
nombre = "Alvar"
```

Esto puede leerse de derecha a izquierda: El string `"Alvar"` es _asignado_ a la variable `nombre`

y cada vez que en un programa escriba `nombre` se va a interpretar como `"Alvar"`

Así, la línea

```python
print(f'{nombre}')
```

Se imprime en el shell de Python

![](/img/pythonVariable.png)

### Actividad 1: 📝 Escribí un Programa que guarde e imprima tu nombre

### Recorrer un string

Un string es un "iterable" es decir, una sucesión de caracteres a los que se puede acceder por su índice. O más específicamente.

Un _Array_ de _bytes_ que representan caracteres _unicode_.

Los array son listas, las podemos pensar así: El string "Hola Mundo" se puede pensar como

| string   | H   | o   | l   | a   |     | M   | u   | n   | d   | o   |
| -------- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| posición | 0   | 1   | 2   | 3   | 4   | 5   | 6   | 7   | 8   | 9   |

Entonces, si hola "Hola mundo" está una variable llamada "mensaje". Podemos acceder a la letra "l" desde su posición así

![](/img/pythonString1.png)

Y como recorro y remplazo letras ❔

Acá aparecen los **bucles**: La sentencia `for` nos sirve para recorrer. Así imprimo en cada renglón las letras del mensaje
e
°[](/img/pythonFor.png)

Esto quiere decir: _para_ cada *carácter\*\* en *mensaje\*\* hacer.

- para = for
- caracter = c
- in = en
- mensaje = "Hola Mundo"

y cada vez que el bucle se repite `c` equivale a 0, después 1, luego 2 y así hasta terminar el string.

### Actividad 2: 📝 Escribí un Programa que escriba las letras de tu nombre en cada renglón

### Cambiando letras

Los string son inmutables, lo que quiere decir que si bien podemos acceder a sus letras, no las podemos cambiar.
Para cambiar la letra `a` del `mensaje` tengo que crear un mensaje nuevo con esa letra cambiada. Por suerte Python tiene _métodos_ que nos ayudan a hacer esto.

![](/img/pythonReplace.png)

Por otro lado, al ser inmutable, si quieren cambiar una letras **y guardar el cambio** tienen que hacerlo en un `string` nuevo. Por ejemplo, acá estoy cambiando cada letra `"L"` por la letra `"M"` y la `"H"` por la `Y`

```python
# Guardo lo que quiero cambiar en la variable mensaje
mensaje = "HOLA MUNDO"
# Creo una variable de TIPO STRING vacia para guardar el nuevo mensaje
mensaje_nuevo = ""

# Recorro la variable mensaje, que es un string. Osea UNA LISTA

for c in mensaje:  # para cada elemento (lo llamo "c") dentro de "mensaje"
    # Si c es igual a L:
    if c == "L":
        letra_nueva = "M"  # Guardo en letra_nueva la nueva letra
    elif c == "H":  # si no, cheque si c es igual a H
        letra_nueva = "Y"  # Si es igual guardo en letra:nueva Y
    else:  # Si no la letra en c no es L, ni H
        letra_nueva = c  # Guardo en letra_nueva lo que haya en c

    # mensaje_nuevo es igual a lo que haya en mensaje nuevo + la letra_nueva
    mensaje_nuevo = mensaje_nuevo + letra_nueva

# Imprimo el mensaje original
print(f"Mensaje original:  {mensaje}")
# Imprimo el menjsaje nuevo
print(f"Mensaje nuevo:  {mensaje_nuevo}")

```

### Actividad 3 📝 Escribí un programa que remplace todas las vocales de tu nombre con números
