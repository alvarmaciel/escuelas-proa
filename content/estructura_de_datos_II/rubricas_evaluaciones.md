---
title: "Rubricas de evaluaciones"
date: 2021-11-25T18:46:07-03:00
draft: false
tags: ["evaluaciones"]
weight : 8
---


Rúbrica de evaluación creada con los estudiantes de Entornos Digitales 2021. Segunda Etapa

![](/images/rubricaTercero.jpg)

|                                                               | Verde                                                                                                                | Amarillo                                                                                                                                                 | Rojo                                                                                                                               |
|---------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------|
| Realización de  las actividades en clase                      | Participo y tengo la intención de realizar las actividades en la clase                                               | Me rindo a mitad de camino o cuando termino molesto al resto                                                                                             | Me la paso hablando de otros temas, no realizo las actividades, molesto a mis compañeras y compañeros                              |
| Comprensión de diferencias entre tipos de datos `int` y `str` | Puedo darme cuenta que tipo de datos usar en cada momento y se que función me permite comvertir datos `int` en `str` | A veces me confundo y me salen errores tipo <br/> `TypeError: unsupported operand type(s) for +: 'int' and 'str'`<br/> entiendo lo que pasa y lo corrijo | No se la diferencia entre esas cosas, el error `TypeError: unsupported operand type(s) for +: 'int' and 'str'` no se que significa |
| Comprensión de métodos de recorrido de `strings`              | Logré usar un ciclo `for` para recorrer un `string` y manipularlo                                                    | Logré manipular un `string` pero no usar el `for`                                                                                                        | No pude manipular el `string` ni usar un `for`                                                                                     |

