---
title: "Proyectos en las Escuelas Proa"
---

# Proyectos en la escuela Proa de Villa Dolores

Esta página contiene los proyectos llevados adelante en la escuela, desde su planificación hasta sus inevitables modificaciones, avances y retrocesos.

{{% notice info %}}
El contenido de este sito está pensado en un doble sentido. Material estático y perdurable para les estudiantes y una bitácora de trabajo que permita a las familias saber qué y como aprenden les chiques.
{{% /notice %}}

## Principios que orientan la práctica

{{% notice tip %}}
Ver [Construccionismo vs Instruccionismo](https://alvarmaciel.gitlab.io/cyberiada/post/construccionismo-vs-instruccionismo1/) de Seymour Papert.
{{% /notice  %}}

El enfoque didáctico que sustenta las propuestas está centrado en el construccionismo como aporte teórico fundamental, el aprendizaje por proyectos y el aprendizaje por indagación. Estos enfoques no cancelan las demostraciones, pero sí ponen acento en la exploración por parte de los estudiantes. Esta exploración no solo es de las herramientas, sino también de las resoluciones mismas de los problemas.

Como toda declaración de principios, no son reglas fijas. Si no más bien orientaciones. Ya que ninguna planificación resiste el primer minuto del aula y ninguna clase es digna sin un plan detrás.

