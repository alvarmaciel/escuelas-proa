---
title: "Ciberseguridad"
date: 2021-11-25T10:38:00-03:00
draft: false
---

A pedido de los estudiantes de sexto año, en la materia de Formación para la vida y el trabajo. Se organizó una serie de charlas. Esta es la que dicté sobre CiberSeguridad.

<iframe frameborder="0" width="1400" height="1000" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true" src="https://alvarmaciel.gitlab.io/escuelas-proa/slides/ciberseguridad/charlaProa.html">
  Fallback text here for unsupporting browsers, of which there are scant few.
</iframe>
