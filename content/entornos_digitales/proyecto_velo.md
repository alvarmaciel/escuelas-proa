+++
title= "Proyecto Velo"
date= 2021-11-08T22:20:21-03:00
draft= false
tags= ['scratch', 'programacion']
+++

Este proyecto se llevó a cabo de forma virtual, en la primer etapa del 2021.

- [Estudio de Scratch con los proyectos](https://scratch.mit.edu/studios/29648195)


## Encuadre narrativo

Se partió de esta narrativa para introducirnos en un encuadre lúdico y narrativo

<iframe src="https://scratch.mit.edu/projects/528177975/embed" allowtransparency="true" width="485" height="402" frameborder="0" scrolling="no" allowfullscreen></iframe>

Este fue le primer desafío.

<iframe src="https://scratch.mit.edu/projects/538172589/embed" allowtransparency="true" width="485" height="402" frameborder="0" scrolling="no" allowfullscreen></iframe>

y este un ejemplo del trabajo de los estudiantes en respuesta al [segundo desafío](https://scratch.mit.edu/projects/557414661)

<iframe src="https://scratch.mit.edu/projects/557653747/embed" allowtransparency="true" width="485" height="402" frameborder="0" scrolling="no" allowfullscreen></iframe>


