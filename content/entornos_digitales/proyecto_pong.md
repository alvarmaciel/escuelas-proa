+++
title= "Proyecto Pong"
date= 2021-11-14T11:23:17-03:00
draft= false
tags= ['scratch', 'programacion']
+++

<div class="tenor-gif-embed" data-postid="16894549" data-share-method="host" data-aspect-ratio="1.20301" data-width="100%"><a href="https://tenor.com/view/pong-video-game-atari-tennis-70s-gif-16894549">Pong Video Game GIF</a>from <a href="https://tenor.com/search/pong-gifs">Pong GIFs</a></div> <script type="text/javascript" async src="https://tenor.com/embed.js"></script>

Durante este proyecto los estudiantes realizarán un juego en [Scratch](https://scratch.mit.edu "Scratch") analizando los requerimientos desde el punto de vista del usuario y escribiendo un producto mínimo viable.

## Etapas

- Escritura de historia de usuario
- Ciclos de programación de 10 minutos con intervalos de 5 minutos para resolver dudas

## Contenidos

- Reconocer patrones en situaciones específicas.
- Identificar la estructura de control ciclo (en cualquiera de sus variantes) para representar patrones de código que se repiten.
- Identificación en el entorno: procesos, ciclos, rutinas que se puedan considerar como algoritmos (concepto intuitivo de algoritmo) y comprender la importancia de organizar en una secuencia lógica un conjunto de pasos sucesivos y organizados.
- Crear programas simples secuenciales utilizando variables, identificando el tipo correspondiente teniendo en cuenta su contenido.
- Crear programas secuenciales simples, utilizando la estructura de control condicional, identificando el rol de la condición para la toma de decisiones.
- Crear programas secuenciales simples, utilizando una estructura de control de repetición.
