+++
title= "Algoritmos de la vida cotidiana"
date= 2021-11-08T22:04:51-03:00
draft= false
tags= ['algoritmos', 'tarea']
weight = 5
+++

Esta actividad es para que puedan ir construyendo el concepto de algoritmo a partir de la práctica.

La realizaron en [Desmos](https://teacher.desmos.com/activitybuilder/custom/609829a19d8aa0066c2eccc4)

Acá les dejo una imagen del documento que acompañaba la actividad y el enlace al doc

![](/images/algoritmos2.png)

- [Algoritmos de la vida cotidiana](https://docs.google.com/document/d/1J4iJsOkUr7ed2ETuGTWS006GsiQerInLydxtMt_7IV0/edit?usp=sharing)

