+++
title= "Rubricas de evaluaciones"
date= 2021-11-19T10:27:09-03:00
draft= false
tags= ["evaluaciones"]
+++

Rúbrica de evaluación creada con los estudiantes de Entornos Digitales 2021. Segunda Etapa

![](/images/rubricaPrimero.jpg)


|  | Logrado | En Proceso | No alcanzado |
|--|---------|------------|--------------|
|Colaboro en clase con mis compañeres | siempre que puedo ayudo con la tarea o resolviendo dudas | no siempre ayudo a mis compañeres| no me gusta ayudar a mis compañeres|
|Me saco las dudas | Siempre pregunto cuando no entiendo | A veces pregunto, pero me quedo con algunas dudas| Me quedo con las dudas|
|Cuido las herramientas| Siempre me ocupo de que las herramientas cumplan con su función y estén en buen estado| a veces me ocupo de que las herramientas estén en buen estado y cumplan su función| No me importan mis herramientas|


¿Qué parte de la programación te gusta más?
